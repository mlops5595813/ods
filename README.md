# ods

### Менеджер пакетов и форматеры

В качестве менеджера пакетов используется pdm. В качестве форматера используется ruff.
Метаданные прописаны в pyproject.toml (в нем также прописаны скрипты для форматирования). 
Зависимости зафиксированы в pdm.lock

Для pre-commit настроен файл .pre-commit-config.yaml

### Создание и запуск контейнера

Для создания и запуска контейнера необходимо воспользоваться командой

```shell
docker-compose up --build
```

Создастся контейнер с названием mlops_app с запущенным jupyterlab на порту 8888.

Зайти в него можно по адресу  http://127.0.0.1:8888/lab?token=docker

С контейнером настроен bind mount: папка src хоста соединена с папкой src контейнера.